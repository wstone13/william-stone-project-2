# Revature Social Network

## Project Description

Revature's Social Network is a browser-based social network application designed to allor users to post messages, add photos, and interact with other users' posts via likes.

## Technologies Used

* Spring
* Java Core
* SQL
* Hibernate
* HMTL
* CSS
* JavaScript
* Log4J
* JUnit
* React
* Redux
* AWS: S3

## Features

List of features ready and TODOs for future development
* Login/Register Functionality
* View home feed
* Search for users
* View user profiles/posts
* View personal user profiles/posts
* Create posts
* Add/change user profile picture
* Update profile information
* Update profile password
* Post liking

To-do list:
* Allow for "unliking" of posts
* Add post commenting
* Allow for multiple photos to be added to posts
* Update visuals
* Create messaging between users
* Add option to delete posts
* Add option to edit posts

## Getting Started
   
(git clone https://gitlab.com/wstone13/william-stone-project-2.git)

> install Tomcat for Java code
> run Tomcat server

- npm install (for React code)

## Usage

> npm start

## Contributors

> Andrew Kellar
> Mark Vargas
> Justin Wen
> Nicholas Deters



