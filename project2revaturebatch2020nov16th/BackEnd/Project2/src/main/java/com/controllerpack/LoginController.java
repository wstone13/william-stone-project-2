package com.controllerpack;

import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.models.Users;
import com.servicepack.UserService;

/**
 * This class serves as the controller for login related endpoints
 * @author Nicholas Deters, Andrew Kellar, William Stone, Mark Anthony Vargas, Justin Wen
 *
 */
@CrossOrigin(origins = "*", allowedHeaders = "*")
@RestController
public class LoginController {
	private UserService userService;

	public LoginController() {
		// TODO Auto-generated constructor stub
	}

	@Autowired
	public LoginController(UserService userService) {
		super();
		this.userService = userService;
	}

	// endpoint for getting all posts from a specified user
	@PostMapping(value = "/login")
	public /* @ResponseBody */ Users login(HttpSession session, @RequestBody Users currentUser) {
		System.out.println("in the login method");
		System.out.println(currentUser);
		currentUser.setPassWord(Integer.toString(currentUser.getPassWord().hashCode()));
		Users loggedUser = userService.getUserByUsername(currentUser.getUserName());
		if (loggedUser == null) {
			System.out.println("In first if");
			return null;
		}
		else if(!currentUser.getPassWord().equals(loggedUser.getPassWord()) ) {
			System.out.println("In second If");
			System.out.println(currentUser.getPassWord() + " " + loggedUser.getPassWord());
			return null;
		}
		else {
			//session.setAttribute("loggedInUser", loggedUser);
			System.out.println("In third If");
			loggedUser.setPassWord("");
			return loggedUser;
		}
	}
}
